# react-native-demo

## Setup
```
sudo npm install -g react-native-cli
sudo npm install -g npx
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt install yarn
react-native init FirstProject
mv FirstProject react-native-demo
cd react-native-demo
echo "sdk.dir = /home/joebert/Android/Sdk" > android/local.properties
react-native start &
react-native run-android
```
