import * as React from 'react';
import { Text, View, StatusBar } from 'react-native';
export default class App extends React.Component {
  state = {
    jsonData: '',
  };
  componentDidMount() {
    fetch('http://192.168.0.18:8000/api/v1/catalog', {
      method: 'GET',
    })
      .then(response => response.json())
      .then(json => {
	products = ''
	for(i=0;i<json.length;i++){
          products += 'Sports: ' + json[i].sports + ' Description: ' + json[i].description + ' Brand: ' + json[i].brand + ' Price: ' + json[i].price + '\n\n'
	}
        this.setState({
          jsonData: products
        });
      })
      .catch(error => {
        console.error(error);
      });
  }
  render() {
    return (
      <View style={{ paddingTop: 30 }}>
        <Text>{this.state.jsonData}</Text>
      </View>
    );
  }
}
